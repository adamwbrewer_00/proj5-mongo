"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2021:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#

# Based on https://rusa.org/pages/acp-brevet-control-times-calculator
# Create tables relative to distance and speed (min & max)
# set as globals so multiple functions can use the tables
min_table = [(200,15.0), (300,15.0), (400,15.0), (600,11.428), (1000,13.333)]
max_table = [(200,34.0), (300,32.0), (400,30.0), (600,28.0), (1000,26.0)]
oddities_table = [(60,20)]


def calculcate_times(distance, brevet_dist_km, speed_table):
    """
    Function should take a given distance and use
    the speed_table to calculcate the time needed.
    """
    i = 0 # use as index for speed_table
    time = 0 # set initial variable for time

    # Based on Example 3
    # When brevets are of distance 1000km and control point > 600
    # (control_dist_km - 600) / speed_600_km occurs
    # to allow for bikers to have more time in
    # extremely long brevets
    if(brevet_dist_km == 1000 and distance > 600):
        for j in range(3):
            time += (200/speed_table[j][1])
            distance -= 200 # allows for looking at distance in 200 km intervals
        time += distance/speed_table[3][1] # distance <= 200

    else:
        # time calculacted by iterating through distance
        # in 200km intervals accordingly with Example 2
        # of Opening Times so that we can consider
        # control points according to where they fall in
        # the speed table.
        while(distance > 200):
            if(i < 4 ):
                time += (200/speed_table[i][1])
                i += 1
                distance -= 200 # allows for looking at distance in 200 km intervals
        time += distance/speed_table[i][1] # distance <= 200

    # returns time in float form, still needs to be formatted
    return time


def format_time(decimal_time, brevet_start_time):

    time_in_minutes = (decimal_time *60)
    hours_t = time_in_minutes // 60
    minutes_t = time_in_minutes - (hours_t * 60) + .5  # add 1/2 for rounding errors
    hours_t += 8 # odd error that is corrected by adding 8 hours

    return arrow.get(brevet_start_time,'YYYY-MM-DD HH:mm').shift(hours =+ hours_t, minutes =+ minutes_t).isoformat()


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    # This is based off Example 1 of Opening times.
    # The final control point gets rolled back to equal
    # the brevet distance to deal with extra distance
    # past the given brevet nominal distance.
    if(brevet_dist_km < control_dist_km):
        control_dist_km = brevet_dist_km

    rough_time = calculcate_times(control_dist_km, brevet_dist_km, max_table)
    fancy_time = format_time(rough_time,brevet_start_time)

    return fancy_time


# trigger goes off if contol point is at 0km
# must be  gloabal incase secondary contol points
# depend on increased time
trigger = 0

def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    # This is based off Example 1 of Opening times.
    # The final control point gets rolled back to equal
    # the brevet distance to deal with extra distance
    # past the given brevet nominal distance.
    if(brevet_dist_km < control_dist_km):
        control_dist_km = brevet_dist_km

    # Used to increment special distance
    if(control_dist_km % 200 == 0):
        increase = control_dist_km // 200

    #this is to account for the 0 value starting point where the original
    #calculator webpage adds an hour on for this part, so follow suit here
    if(control_dist_km == 0):
        fancy_time = format_time(1, brevet_start_time)
        global trigger
        trigger = 1

    # Computes oddites example where contol distances are
    # under 60 km
    elif(0 < control_dist_km < 61):
        rough_time = calculcate_times(control_dist_km, brevet_dist_km, oddities_table)
        fancy_time = format_time(rough_time+trigger,brevet_start_time)

    # Found through testing that brevet contol points equal to
    # or greater than brevet distance get extra (10*(brevet_dist_km // 200) minutes added
    # to close time if brevet_dist_km < 600
    elif(control_dist_km == brevet_dist_km and brevet_dist_km < 600):
        rough_time = calculcate_times(control_dist_km, brevet_dist_km, min_table)
        fancy_time = format_time(rough_time+(increase/6),brevet_start_time)

    # General Closing times
    else:
        rough_time = calculcate_times(control_dist_km, brevet_dist_km, min_table)
        fancy_time = format_time(rough_time,brevet_start_time)

    return fancy_time
