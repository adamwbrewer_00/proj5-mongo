# Project 5: Brevet time calculator with Ajax and MongoDB

Simple list of controle times from project 4 stored in MongoDB database.

## Author: Adam Brewer , abrewer2@uoregon.edu
Credits to Michal Young and Ram Durairajan for the initial version of this code.

## ACP controle times
The algorithm for calculating controle times is described here (https://rusa.org/pages/acp-brevet-control-times-calculator).  
Additional background information is given here (https://rusa.org/pages/rulesForRiders).  
We are essentially replacing the calculator here (https://rusa.org/octime_acp.html).

## About Brevets  
Randonneuring is long distance non-competive cycling. A cycling event for randonneurs is known as a "Brevet". "Controls" are checkpoints within a brevet. This calculator calculates the open and close times for a brevet and its controls.

## How to Run  
Fork proj5-mongo to your own repository.  
Then clone this repository onto your working machine.  
Then launch docker on your machine.  
Launch localhost:5000 on an open browser of yours.  
Then from a linux terminal, enter your cloned repository.  
Enter the DockerMongo folder and run the command "./run.sh" on a linux terminal to boot up the calculator.
Refresh you browser window and you should see a template render.
To terminate process, run the command "./stop.sh".

## How to use Calculator  
First enter the brevet overall distance in the Distance drop down menu.  
Only certain distances are allowed based off of RUSA ACP standards.  
Then enter the beginning time and date in the "Begins at" drop down menus.  
From here one can enter in control points in the miles or km space from top to bottom.  
This should output open and close times based off the calculator.  

## Calculator Rules and Oddities
All open and closed time are based off the table given by the RUSA ACP according to the brevet overall distance and speed min & max.  
If you input distances far larger than the brevet overall distance the calculator will output incorrect times.   
However, if the final control point is under 120% of the overall distance no errors will occur. Any control points greater than overall brevet distance will be set to brevet distance in calculator.  
If a control point is set a 0km, and additional hour is added to the any control point under 61km.  
If control point is under 600 and equal to or greater than brevet overall distance than time will increase by 10 minutes for 200km and 20 min for 400km.  
If the brevet distance is 1000km, any control point over 600 will be divided by the speed associated with 600km to allow bikers to have longer time to arrive as the distance is much larger.  
All minutes calculated will be rounded up.

## Test Cases  
1) If no data is entered into the calculator, and the user clicks on the "Submit" button, then clicks on the "Display" button, a submit error html page will pop up saying no data was entered and ask the user to try to enter data.  

2) After the client inputs data, clicks on "Submit", then clicks on "Display". If a controle point is entered over 20% of the overall brevet distance a large number error will occur telling the user the controle point that was entered is too large in comparison to the overall brevet distance which is based on the original ACP Brevet calculator.  
